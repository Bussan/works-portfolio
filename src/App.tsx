import React from "react";
import "./App.css";
import { StyledMainGrid } from "./App.styled";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import { Home, PortfolioItem } from "./pages";
import Footer from "./components/footer/Footer";
import NavBar from "./components/navigation/navbar/Navbar";

function App() {
  return (
    <StyledMainGrid>
      <Router>
        <NavBar />
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/:pageId" element={<PortfolioItem />} />
        </Routes>
      </Router>
      <Footer />
    </StyledMainGrid>
  );
}

export default App;
