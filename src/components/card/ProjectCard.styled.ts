import styled from "styled-components";

export const StyledProjectCard = styled.figure`
  /* display: block; */
  position: relative;
  min-height: 200px;
  max-height: 500px;
  min-width: 200px;
  /* max-width: 500px; */
  margin: 0;

  img {
    display: block;
    width: 100%;
    height: 100%;
    object-fit: cover;
    background-position: center center;
  }
  figcaption {
    position: absolute;
    top: 50%;
    left: 50%;
    width: 100%;
    opacity: 0;
    text-align: center;
    color: white;
    will-change: transform;
    backface-visibility: hidden;
    transform: translate(-50%, -50%) scale(0.9);
    transition: all 0.5s;
  }

  a {
    position: absolute;
    top: 0;
    bottom: 0;
    left: 0;
    right: 0;
    width: 100%;
    height: 100%;
    z-index: 99999;
    text-indent: 200%;
    white-space: nowrap;
    font-size: 0;
    opacity: 0;
    cursor: pointer;
  }

  h3 {
    font-size: 32px;
    font-weight: 800;
    line-height: 1;
    margin-bottom: 0;

    &:before,
    &:after {
      display: inline-block;
      margin: 0 0.5em;
      width: 0.75em;
      height: 0.03em;
      background: #ffffff;
      content: "";
      vertical-align: middle;
      transition: all 0.3s;
    }
  }

  p {
    font-weight: 400;
    font-size: 16px;
    line-height: 1.2;
    color: #ffffff;
  }

  &:hover {
    background-color: #ff0266;
    .img:before,
    .img:after,
    figcaption {
      opacity: 1;
    }
    img {
      opacity: 0.3;
    }

    figcaption {
      transform: translate(-50%, -50%) scale(1);
    }
  }
`;
