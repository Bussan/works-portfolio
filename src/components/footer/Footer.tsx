import React from "react";
import { StyledFooterOutter } from "./Footer.styled";

function Footer(): JSX.Element {
  return (
    <StyledFooterOutter>
      <div>
        <h4>Email</h4>
        <p>
          <a href="mailto:bpdemont.design+portfolio@gmail.com?subject=Project Inquiry">
            bpdemont.design@gmail.com
          </a>
        </p>
      </div>

      <div>
        <h4>Social</h4>
        <ul>
          <li>
            <a
              href="https://jp.linkedin.com/in/bryan-demont"
              target="_blank"
              rel="noreferrer"
            >
              Linkedin
            </a>
          </li>
          <li>
            <a
              href="https://www.facebook.com/bryan.demont.1/"
              target="_blank"
              rel="noreferrer"
            >
              Facebook
            </a>
          </li>
        </ul>
      </div>
      <div>
        <h4>Location</h4>
        <p>Yokohama, Japan</p>
      </div>
    </StyledFooterOutter>
  );
}
export default Footer;
