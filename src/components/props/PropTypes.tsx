export default interface PropTypes {
  // URL
  pageId: string;

  // Project Card
  cardTitle: string;
  cardSubtitle: string;
  cardImg: string;

  // Portfolio Item Page
  title: string;
  subtitle: string;
  body: string;
  img1: string;
  img2: string;
  img3: string;
  img4: string;
  img5: string;

  // Portfolio short data
  companyName: string;
  projectTools1: string;
  projectTools2: string;
  projectTools3: string;
  projectTools4: string;
  projectTools5: string;
  projectDate: string;
  projectType: string;
  projectTypeAdd: string;

  // Portfolio Content
  section1Header: string;
  section2Header: string;
  section3Header: string;
  section4Header: string;
  section5Header: string;
  section1Body: string;
  section2Body: string;
  section3Body: string;
  section4Body: string;
  section5Body: string;
}
