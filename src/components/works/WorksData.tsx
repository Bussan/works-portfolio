import PropTypes from "../props/PropTypes";

import DashBoardImg from "../../assets/dashboard.png";
import HanabiCard from "../../assets/hanabi-glass.jpg";
import HanabiLogo from "../../assets/hanabi-logo.jpg";
import HanabiProduct from "../../assets/hanabi-flat-lay.jpg";

// export const genBlogId = (): string => Math.random().toString(36).slice(-6);

export const WorksData: PropTypes[] = [
  {
    pageId: "hanabi-glass",

    cardTitle: "Hanabi Glass",

    cardSubtitle: "Luxury Glass Pens",

    cardImg: HanabiCard,

    title: "Hanabi Glass",

    subtitle: "Logo creation facilitating change in brand identiy",

    companyName: "Hanabi Glass Studio",

    projectType: "Logo Design",

    projectTypeAdd: "Brand Strategy",

    projectDate: "2016",

    projectTools1: "Adobe Illustrator",

    projectTools2: "Pen & Paper",

    projectTools3: "",

    projectTools4: "",

    projectTools5: "",

    body: "Hanabi Glass Studio is a small lampworking shop owned by Lucas Mahoney. His initial request for a logo turned into creating a brand and pivioting his business plan to target his target audience. The project involved the design of a logo to match his needs along with the creation of a brand strategy (not detailed here) that would allow him to create glass artwork fulltime without the need of supplemental income.",

    section1Header: "The client's needs",

    section1Body:
      "During the initial meeting to discuss the creation of the logo we discussed the shops needs for a logo. To better understand his business and community surrounding his work, we discussed lamp working and the niche he was currently targeting. Once I had a better understanding of his business we discussed how he wanted his business to evolve and the value he expects the logo to add. While it wasn't currently what he was doing, he wanted to target an audience that would be buying his works from botique shops and wanted to move towards the luxury segement.",

    section2Header: "Defining the task",

    section2Body:
      "The goal after our initial discussion was to create a logo that would work in the following places. First it needed to be a logo that would work well on luxury packaging and not look out of place in a highend art boutique. Second, as currently the main sales are done through Instagram, the logo needed to be circular and easily recognizable. Lastly the logo needed to combine glasswork, fireworks company's name sake), and Japan.",

    section3Header: "Creation",

    section3Body:
      "During the Ideate phase of my process I spent time researching not only other glass shops but also luxury items sold by other companies that would be comparable to glass items, jewlery, and artwork. From this I learned that a common item made, collected, and sold of high prices in the lamp working world is marbles. In this artistic community marbles are used to showcase and artists skill and are considered limited in makes and highly collectable. Adding this to the fact that Instagrams profile photos are circular, the idea of a marble as the logo was solidfied. As the shop is called 'Hanabi Glass' (fireworks glass) I spoke with the client and he agreed that a firework design on a marble similar to a Japanese firework motif found on Yukata during fireworks festivals would be the goal for the logo's final design. After about 30 iterations of the design, a final logo was given the okay.",

    section4Header: "Logo .v4 .final .v2 .final .done",

    section4Body:
      "While the logo's general idea had been finalized it still needed to be created and tested across different mediums. The logo was created in illustrator and was tested on various mockups in Photoshop. Slight modifications had to be made to simplfy the logo as well as made it stand out a bit more.",

    section5Header: "Testing the logo",

    section5Body:
      "Before finalizing the design I tested the logo in various use cases. The logo was tested inverted, small, big, on business cards, clothing, packaging, and on different colored backgrounds. Once the logo was useable in each of these different ways while being easily recognizable, the final design was submitted to the client and soon after approved. The final files were created and deliverd to the client so that they can easily use the logo on social media, for print purposes, or to send to other shops for clothing and or packaging creating purposes.",

    img1: HanabiLogo,
    img2: "",
    img3: "",
    img4: "",
    img5: HanabiProduct,
  },
  {
    pageId: "realestate",

    cardTitle: "Real Estate",

    cardSubtitle: "Website Redesign",

    cardImg:
      "https://images.unsplash.com/photo-1531219572328-a0171b4448a3?q=80&w=2670&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D",

    title: "PLAZA HOMES",

    subtitle: "Website Redesign and Frontend development",

    companyName: "PLAZA HOMES",

    projectType: "Web design",

    projectTypeAdd: "Frontend Development",

    projectDate: "2018",

    projectTools1: "Adobe Photoshop",

    projectTools2: "Adobe Illustrator",

    projectTools3: "Visual Studio Code",

    projectTools4: "Codekit",

    projectTools5: "Pug, SCSS",

    body: "PLAZA HOMES is a real estate agency located in Tokyo specializing in foreign clientele. I was hired with the goal of redesesgining their website to match the taste and current web design trends used outside of Japan. This also includes improving the usability as well as the sites speed, access amount, and SEO.",

    section1Header: "Understanding",

    section1Body:
      "At the start of the project I spent time speaking with the site stakeholders to gain a better understanding of their goals for the site as well as what they felt it was lacking. After that I researched their competition as well as other sites in the same industry that were commonly used in America, Canada, and Austrailia. Once I had a general Idea of what was being done by the competitors, I spent time breaking down the sites structure and making user flows to get a better understanding of what they currently had and what could be used, modified, or had to go.",

    section2Header: "Defining the goals",

    section2Body: "",

    section3Header: "Ideate & Prototype",

    section3Body:
      "Initially I made various mockups of three main pages in various styles. The site actually had around 76 different layout patterns throughout the sites 200 plus pages. Once we discussed and talked about the strengths and weaknesses of the 10 different designs I produced a decision was made. I then began building out the intial wireframes for each of the different page layouts followed by the ui components. During this entire process I had to keep in mind the limits of the CMS the company was using and not design for features the CMS was unable to support as I would be coding the site as well.",

    section4Header: "Test",

    section4Body:
      "Once the designs were finished, I began coding each componenet and page. Each one was added to the companies preferred CMS which was unfortunately unable to support modern HTML 5 coding practices. As each page was made, constant testing for accessibility, speed optimization, and responsiveness was done.",

    section5Header: "Implementation",

    section5Body:
      "Upon completion of the site in 2018, the site was made live. During this time we were able to assesss what improvements had been made over the previous site. Access and time spent on main pages increased by 40%. As SEO was optimized during my coding, the pages ranking when searching for our keywords brought the company from page 3 and 4 to number 10 on page 1 for the majority of keywords. After the sites completion, I made similar sites for the companies subsidiaries (All Japan Relocation, Livining in Japan) and a partnert company (Maeda Real Estate).",

    img1: "",
    img2: "",
    img3: "",
    img4: "",
    img5: "",
  },
  {
    pageId: "dashboard",

    cardTitle: "Dashboard",

    cardSubtitle: "Internal Dashboard Redesign",

    cardImg:
      "https://images.unsplash.com/photo-1504868584819-f8e8b4b6d7e3?q=80&w=2076&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D",

    title: "Dashboard",

    subtitle: "Aggregrated Data Dashboard",

    companyName: "Undisclosed",

    projectType: "Design",

    projectTypeAdd: "UX/UI",

    projectDate: "2022 - 2023",

    projectTools1: "Figma",

    projectTools2: "Adobe Illustrator",

    projectTools3: "VS Code",

    projectTools4: "GitLab",

    projectTools5: "Design Thinking Framework",

    body: "The goal of this project was to re-design an internal tools dashboard and improve the user experience. Some of the main problems the client was looking to solve were; inconsistent design, steep learning curve, reduce help desk requests, increase number of users, and reduce the time needed to train users to properly use the tool. The biggest challenge was understanding the tool on a deeper level so that I could help rebuild and modify user flows as well as create a more intuitive user experience along with a consistent and visually pleasing user interface.",

    section1Header: "Diagnostic Analysis",

    section1Body:
      "Conducted a comprehensive analysis of the original dashboard website, focusing on usability, consistency, and accessibility. Broke down the UI into parts and categorized them based on their functions and locations. Identified issues with poor organization, lack of consistency, small fonts, and poor accessibility.",

    section2Header: "Stakeholder Collaboration",

    section2Body:
      "Collaborated closely with the project team, app developers, and employees who rely on the tool to understand their goals and objectives. Gathered feedback from stakeholders on pain points and areas for improvement. Through extensive communication I was able to gain a deeper understading of the product as well as the thinking and goals behind it's original creation.",

    section3Header: "Iterative Redesign Process",

    section3Body:
      "Redesigned the existing dashboard to prioritize accessibility and consistency, addressing font sizes, contrast ratios, and layout organization. Implemented new features such as advanced search functionality to improve user experience and efficiency. Introduced features like the ability to hide columns and organize data tables for better data visualization and user interaction.",

    section4Header: "Content Migration and Optimization",

    section4Body:
      "Managed content migration through version control, ensuring a seamless transition to the redesigned dashboard. Implemented changes gradually after thorough review in development environments, maintaining data integrity and accuracy. No changes were made to the content strategy, focusing instead on improving usability and functionality.",

    section5Header: "Launch and Continuous Improvement",

    section5Body:
      "Oversaw the launch of the redesigned dashboard, ensuring a smooth transition for users. Monitored user feedback and analytics post-launch to assess the impact of the redesign. Observed a significant improvement in the ease of use of the dashboard, resulting in a 71% decrease in tech support calls for assistance. Used feedback to iterate and improve other necessary user flows in turn increasing user number and lowering help desk inquiries.",

    img1: DashBoardImg,
    img2: "",
    img3: "",
    img4: "",
    img5: "",
  },
  {
    pageId: "casestudy",

    cardTitle: "Case Study",

    cardSubtitle: "In progress",

    cardImg:
      "https://images.unsplash.com/photo-1512758017271-d7b84c2113f1?q=80&w=2070&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D",

    title: "UX Case Study",

    subtitle: "in progress",

    companyName: "Julius",

    projectType: "Design",

    projectTypeAdd: "UX",

    projectDate: "2024",

    projectTools1: "Figma",

    projectTools2: "Google Docs",

    projectTools3: "Google Sheets",

    projectTools4: "",

    projectTools5: "",

    body: "",

    section1Header: "",

    section1Body: "",

    section2Header: "",

    section2Body: "",

    section3Header: "",

    section3Body: "",

    section4Header: "",

    section4Body: "",

    section5Header: "",

    section5Body: "",

    img1: "",
    img2: "",
    img3: "",
    img4: "",
    img5: "",
  },
];
