// import { solutions } from "../assets/images";
import React from "react";
import { useParams } from "react-router-dom";
import { WorksData } from "../../components/works/WorksData";
import {
  StyledPageGrid,
  StyledItemTitle,
  StyledItemSubtitle,
  StyledItemDescription,
  StyledCompanyInfo,
  StyledItemImages,
  StyledProblems,
  StyledResults,
  StyledSolution,
  StyledSection4,
  StyledSection5,
  StyledContent,
} from "./ItemDescription.styled";

export default function ItemDescription() {
  const { pageId } = useParams();
  const works = WorksData.filter((data) => data.pageId === pageId)[0];

  console.log(pageId);
  console.log(WorksData[0].pageId);

  return (
    <div>
      <StyledPageGrid>
        <StyledItemTitle>
          <h1>{works.title}</h1>
        </StyledItemTitle>

        <StyledItemSubtitle>
          <h2>{works.subtitle}</h2>
        </StyledItemSubtitle>

        <StyledCompanyInfo>
          <h4>Client</h4>
          <ul>
            <li>{works.companyName}</li>
            <li>{works.projectDate}</li>
            <li>{works.projectType}</li>
            <li>{works.projectTypeAdd}</li>
          </ul>

          <h4>Tools</h4>
          <ul>
            <li>{works.projectTools1}</li>
            <li>{works.projectTools2}</li>
            <li>{works.projectTools3}</li>
            <li>{works.projectTools4}</li>
            <li>{works.projectTools5}</li>
          </ul>
        </StyledCompanyInfo>

        <StyledItemDescription>
          <p>{works.body}</p>
        </StyledItemDescription>

        <div className="empty"></div>

        {/* Section 1 */}
        <StyledProblems>
          <StyledItemImages>
            <figure>
              <img src={works.img1} />
              <figcaption></figcaption>
            </figure>
          </StyledItemImages>
          <StyledContent>
            <h2>{works.section1Header}</h2>
            <p>{works.section1Body}</p>
          </StyledContent>
        </StyledProblems>

        {/* Section 2 */}
        <StyledResults>
          <StyledItemImages>
            <figure>
              <img src={works.img2} />
              <figcaption></figcaption>
            </figure>
          </StyledItemImages>
          <StyledContent>
            <h2>{works.section2Header}</h2>
            <p>{works.section2Body}</p>
          </StyledContent>
        </StyledResults>

        {/* Section 3 */}
        <StyledSolution>
          <StyledItemImages>
            <figure>
              <img src={works.img3} />
              <figcaption></figcaption>
            </figure>
          </StyledItemImages>
          <StyledContent>
            <h2>{works.section3Header}</h2>
            <p>{works.section3Body}</p>
          </StyledContent>
        </StyledSolution>

        {/* Section 4 */}
        <StyledSection4>
          <StyledItemImages>
            <figure>
              <img src={works.img4} />
              <figcaption></figcaption>
            </figure>
          </StyledItemImages>
          <StyledContent>
            <h2>{works.section4Header}</h2>
            <p>{works.section4Body}</p>
          </StyledContent>
        </StyledSection4>

        {/* Section 5 */}
        <StyledSection5>
          <StyledItemImages>
            <figure>
              <img src={works.img5} />
              <figcaption></figcaption>
            </figure>
          </StyledItemImages>
          <StyledContent>
            <h2>{works.section5Header}</h2>
            <p>{works.section5Body}</p>
          </StyledContent>
        </StyledSection5>
      </StyledPageGrid>
    </div>
  );
}
