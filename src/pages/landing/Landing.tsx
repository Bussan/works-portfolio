import React from "react";
import { WorksData } from "../../components/works/WorksData";
import ProjectCard from "../../components/card/ProjectCard";
import { StyledPageGrid } from "./Landing.styled";

function Landing(): JSX.Element {
  return (
    <StyledPageGrid>
      {/* Top Page */}
      <article>
        <h1>Full-stack Design</h1>
        <h2>Coding, Design, & User Experience</h2>
        <p>
          From research to stragey to implementation. A jack of all trades when
          it comes to web design.
        </p>
      </article>

      {/* This takes the data from the WorksData.tsx and uses the props in the brackets to connect the (item) to each of the objects values and display them in a card.  */}

      {/* The map() function is used to iterate over each object in the WorksData array */}
      {WorksData.map((item) => (
        <ProjectCard
          src={item.cardImg}
          title={item.cardTitle}
          subtitle={item.cardSubtitle}
          pageId={item.pageId}
          key={item.pageId}
        />
      ))}
    </StyledPageGrid>
  );
}
export default Landing;
